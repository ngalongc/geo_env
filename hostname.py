import socket
def get_host_name(hostname):
    addr = socket.gethostbyname(hostname)
    print('The IP address of {} is {}'.format(hostname, addr))
if __name__ == "__main__":
    get_host_name('www.google.com')
    get_host_name('www.babysbreathhk.com')

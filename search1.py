#!/usr/bin/env python3

from pygeocoder import Geocoder

if __name__ == "__main__":
    address1 = '207 N.Defiance St, Archbold, OH'
    address2 = 'Constellation Cove Hong Kong'
    address3 = 'Wai Wah Industrail Building Pak Tin Par St. Tsuen Wan\
    Hong Kong'
    address4 = 'Mei Zhou, wing Ci Zipper Factory'
    print(Geocoder.geocode(address1)[0].coordinates)
    print(Geocoder.geocode(address2)[0].coordinates)
    print(Geocoder.geocode(address3)[0].coordinates)
    print(Geocoder.geocode(address4)[0].coordinates)

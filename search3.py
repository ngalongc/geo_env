#! /usr/bin/env python3

import http.client
import json
from urllib.parse import quote_plus

base = '/maps/api/geocode/json'
def geocode(address):
    path = '{}?address={}&sensor=false'.format(base, quote_plus(address))
    connection = http.client.HTTPConnection('maps.google.com')
    connection.request('GET', path)
    rawreply = connection.getresponse().read()
    reply = json.loads(rawreply.decode('utf-8'))
    #json help the rawreply turn from json format to dict format
    print(reply['results'][0]['geometry']['location'])
    print('*'*100)
    print(type(rawreply))
    print('*'*100)
    print(type(reply))

if __name__ == "__main__":
    geocode('constellation cove tai po hong kong')
